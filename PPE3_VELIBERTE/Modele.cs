﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Data;
using System.Windows.Forms;

namespace PPE3_VELIBERTE
{
    /// <summary>
    /// classe Modele : gestion des données de la BD bd_ppe3_veliberte    
    /// </summary>
    public class Modele
    {
        #region propriétés

        private MySqlConnection myConnection;   // objet de connexion
        private bool connopen = false;          // test si la connexion est faite
        private bool errgrave = false;          // test si erreur lors de la connexion
        private bool chargement = false;        // test si le chargement d'une requête est fait

        // les DataAdapter et DataTable seront gérés dans des collections avec pour chaque un indice correspondant :
        // indice 0 : récupération des noms des tables
        // indice 1 : Table borne
        // indice 2 : Table adherent
        // indice 3 : Table ... à vous de créer la suite

        // collection de DataAdapter
        private List<MySqlDataAdapter> dA = new List<MySqlDataAdapter>();

        // collection de DataTable récupérant les données correspond au DA associé
        private List<DataTable> dT = new List<DataTable>();

        #endregion

        #region accesseurs
        /// <summary>
        /// test si la connexion à la BD est ouverte
        /// </summary>
        public bool Connopen
        {
            get { return connopen; }
            set { connopen = value; }
        }

        /// <summary>
        /// test si erreur lors de la connexion
        /// </summary>
        public bool Errgrave
        {
            get { return errgrave; }
            set { errgrave = value; }
        }

        /// <summary>
        /// test si le chargement d'une requête est fait
        /// </summary>
        public bool Chargement
        {
            get { return chargement; }
            set { chargement = value; }
        }

        /// <summary>
        /// Accesseur de la collection des DataAdapter
        /// </summary>
        public List<MySqlDataAdapter> DA
        {
            get { return dA; }
            set { dA = value; }
        }

        /// <summary>
        /// Accesseur de la collection des DataTable
        /// </summary>
        public List<DataTable> DT
        {
            get { return dT; }
            set { dT = value; }
        }
        #endregion

        /// <summary>
        /// Modele() : constructeur permettant l'ajout des DataAdpater et DataTable nécessaires (4 nécessaires pour l'existant actuel)
        /// indice 0 : récupération des noms des tables
        /// indice 1 : Table borne
        /// indice 2 : Table adherent
        /// indice 3 : Table ...
        /// </summary>
        public Modele()
        {

            for (int i = 0; i < 9 ; i++)
            {
                dA.Add(new MySqlDataAdapter());
                dT.Add(new DataTable());
            }
        }

        /// <summary>
        /// méthode seconnecter permettant la connexion à la BD : bd_ppe3_notagame
        /// </summary>
        public void seconnecter()
        {
            string myConnectionString = "Database=ppe;Data Source=192.168.151.1;User Id=root;Password=root;"; //SslMode=none
            myConnection = new MySqlConnection(myConnectionString);
            try // tentative 
            {
                myConnection.Open();
                connopen = true;
            }
            catch (Exception err)// gestion des erreurs
            {
                MessageBox.Show("Erreur ouverture BD Veliberte : " + err, "PBS connection", MessageBoxButtons.OK, MessageBoxIcon.Error);
                connopen = false; errgrave = true;
            }
        }

        /// <summary>
        /// méthode sedeconnecter pour se déconnecter à la BD
        /// </summary>
        public void sedeconnecter()
        {
            if (!connopen)
                return;
            try
            {
                myConnection.Close();
                myConnection.Dispose();
                connopen = false;
            }
            catch (Exception err)
            {
                MessageBox.Show("Erreur fermeture BD Veliberte : " + err, "PBS deconnection", MessageBoxButtons.OK, MessageBoxIcon.Error);
                errgrave = true;
            }
        }

        /// <summary>
        /// méthode générique privée pour charger le résultat d'une requête dans un dataTable via un dataAdapter
        /// Méthode appelée par charger_donnees(string table)
        /// </summary>
        /// <param name="requete">requete à charger</param>
        /// <param name="DT">dataTable</param>
        /// <param name="DA">dataAdapter</param>
        private void charger(string requete, DataTable DT, MySqlDataAdapter DA)
        {
            DA.SelectCommand = new MySqlCommand(requete, myConnection);

            // pour spécifier les instructions de mise à jour (insert, delete, update)
            MySqlCommandBuilder CB1 = new MySqlCommandBuilder(DA);
            try
            {
                DT.Clear();
                DA.Fill(DT);
                chargement = true;
            }
            catch (Exception err)
            {
                MessageBox.Show("Erreur chargement dataTable : " + err, "PBS table", MessageBoxButtons.OK, MessageBoxIcon.Error);
                errgrave = true;
            }
        }

        /// <summary>
        /// charge dans un DT les données de la table passée en paramètre
        /// </summary>
        /// <param name="table">nom de la table à requêter</param>
        public void charger_donnees(string table)
        {
            chargement = false;
            if (!connopen) return;		// pour vérifier que la BD est bien ouverte

            if (table == "public")
            {
                charger("SELECT table_name FROM information_schema.tables WHERE table_schema = 'ppe' AND table_name NOT LIKE 'reparer' AND table_name NOT LIKE 'travaux' AND table_name NOT LIKE 'utilisateur' AND table_name NOT LIKE 'louer';", dT[0], dA[0]);
            }

            if (table == "borne")
            {
                charger("select * from borne;", dT[1], dA[1]);
            }

            if (table == "adherent")
            {
                charger("select * from adherent;", dT[2], dA[2]);
            }

            if (table == "vehicule")
            {
                charger("select * from vehicule", dT[3], dA[3]);
            }

            if (table == "velo")
            {
                charger("select * from velo;", dT[4], dA[4]);
            }

            if (table == "veloelectrique")
            {
                //charger("SELECT ve.numV, ve.numB, b.nomB FROM veloelectrique AS ve INNER JOIN borne AS b WHERE b.codeB = ve.numB;", dT[5], dA[5]); 
                charger("select * from veloelectrique;", dT[5], dA[5]);
            }

            if (table == "technique")
            {
                charger("show tables;", dT[6], dA[6]);
            }

            if (table == "travaux")
            {
                charger("select * from travaux;", dT[7], dA[7]);
            }

            if (table == "reparer")
            {
                charger("select * from reparer;", dT[8], dA[8]);
            }
        }

        public List<string> liste_vehicule()
        {
            List<string> vehicules = new List<string>();

            MySqlCommand cmd = new MySqlCommand("SELECT vehicule.numV FROM vehicule WHERE vehicule.numV NOT IN(SELECT velo.numV FROM velo) AND vehicule.numV NOT IN(SELECT veloelectrique.numV FROM veloelectrique); ", myConnection);
            MySqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                vehicules.Add(reader.GetString(0));
            }

            reader.Close();

            return vehicules;
        }

        public List<string> liste_borne()
        {
            List<string> bornes = new List<string>();

            MySqlCommand cmd = new MySqlCommand("SELECT nomB FROM borne", myConnection);
            MySqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                bornes.Add(reader.GetString(0));
            }

            reader.Close();

            return bornes;
        }

        public int id_borne(string nomB)
        {
            int id = 0;

            MySqlCommand cmd = new MySqlCommand("SELECT codeB FROM borne WHERE nomB = @nom", myConnection);

            cmd.Parameters.AddWithValue("@nom", nomB);

            MySqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                id = reader.GetInt32(0);
            }

            reader.Close();

            return id;
        }

        public string nom_borne(int id)
        {
            string nom = "";

            MySqlCommand cmd = new MySqlCommand("SELECT nomB FROM borne WHERE codeB = @id", myConnection);

            cmd.Parameters.AddWithValue("@id", id);

            MySqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                nom = reader.GetString(0);
            }

            reader.Close();

            return nom;
        }

        public bool verif_connexion(string login, string password)
        {
            MySqlCommand cmd = new MySqlCommand("SELECT * FROM utilisateur WHERE loginU = @login AND passwordU = @password", myConnection);

            cmd.Parameters.AddWithValue("@login", login);
            cmd.Parameters.AddWithValue("@password", password);

            MySqlDataReader reader = cmd.ExecuteReader();

            if (reader.Read())
            {
                reader.Close();
                return true;
            }

            reader.Close();
            return false;
        }

        public List<int> liste_numV_reparer()
        {
            List<int> num_reparations = new List<int>();

            MySqlCommand cmd = new MySqlCommand("SELECT DISTINCT numV FROM reparer", myConnection);
            MySqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                num_reparations.Add(reader.GetInt32(0));
            }

            reader.Close();

            return num_reparations;
        }

        public List<string> liste_reparation(int num)
        {
            List<string> reparations = new List<string>();

            MySqlCommand cmd = new MySqlCommand("SELECT * FROM reparer AS r INNER JOIN travaux AS t ON t.IdT = r.IdT WHERE r.numV = @num", myConnection);
            cmd.Parameters.AddWithValue("@num", num);

            MySqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                 reparations.Add(reader.GetString("LibelleT"));
            }

            reader.Close();

            return reparations;
        }

        public int temps_total_reparation(int num)
        {
            int sum = 0;

            MySqlCommand cmd = new MySqlCommand("SELECT SUM(tempsR) FROM reparer WHERE numV = @num", myConnection);
            cmd.Parameters.AddWithValue("@num", num);

            MySqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                sum = reader.GetInt32(0);
            }

            reader.Close();

            return sum;
        }
    }
}
