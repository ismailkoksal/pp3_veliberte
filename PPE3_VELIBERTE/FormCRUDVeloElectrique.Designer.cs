﻿using System.Windows.Forms;

namespace PPE3_VELIBERTE
{
    partial class FormCRUDVeloElectrique
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label3 = new System.Windows.Forms.Label();
            this.cbNumV = new System.Windows.Forms.ComboBox();
            this.cbNumB = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnOK = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.label3.Location = new System.Drawing.Point(25, 43);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(155, 16);
            this.label3.TabIndex = 10;
            this.label3.Text = "Numéro Vélo électrique :";
            this.label3.Visible = false;
            // 
            // cbNumV
            // 
            this.cbNumV.FormattingEnabled = true;
            this.cbNumV.Location = new System.Drawing.Point(186, 42);
            this.cbNumV.Name = "cbNumV";
            this.cbNumV.Size = new System.Drawing.Size(190, 21);
            this.cbNumV.TabIndex = 11;
            this.cbNumV.Visible = false;
            // 
            // cbNumB
            // 
            this.cbNumB.FormattingEnabled = true;
            this.cbNumB.Location = new System.Drawing.Point(186, 69);
            this.cbNumB.Name = "cbNumB";
            this.cbNumB.Size = new System.Drawing.Size(190, 21);
            this.cbNumB.TabIndex = 12;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.label1.Location = new System.Drawing.Point(25, 70);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(143, 16);
            this.label1.TabIndex = 13;
            this.label1.Text = "Borne Vélo électrique :";
            // 
            // btnOK
            // 
            this.btnOK.Cursor = System.Windows.Forms.Cursors.Default;
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnOK.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOK.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btnOK.Location = new System.Drawing.Point(167, 128);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 35);
            this.btnOK.TabIndex = 14;
            this.btnOK.Text = "OK\r\n";
            this.btnOK.UseVisualStyleBackColor = true;
            // 
            // FormCRUDVeloElectrique
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(410, 197);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbNumB);
            this.Controls.Add(this.cbNumV);
            this.Controls.Add(this.label3);
            this.Name = "FormCRUDVeloElectrique";
            this.Text = "FormVeloElectrique";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbNumV;
        private ComboBox cbNumB;
        private Label label1;
        private Button btnOK;

        public Label Label3
        {
            get
            {
                return label3;
            }

            set
            {
                label3 = value;
            }
        }

        public ComboBox CbNumV
        {
            get
            {
                return cbNumV;
            }

            set
            {
                cbNumV = value;
            }
        }

        public ComboBox CbNumB
        {
            get
            {
                return cbNumB;
            }

            set
            {
                cbNumB = value;
            }
        }
    }
}